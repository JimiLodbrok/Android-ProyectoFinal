package com.example.mylibrary.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.example.mylibrary.R;
import com.example.mylibrary.events.EventsAdmin;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.internal.TwitterApi;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    private Button btnLogin, btnRegister;
    private TextInputEditText edtxt_User, edtxt_Password;
    private String firstName, lastName, birthday, gender, userId;
    private String fullname,profilePictureS,email;
    private URL profilePicture;
    private LoginButton btnFacebook;
    private CallbackManager callbackManager;
    private FacebookListener listener;
    private TwitterListener listenerTwitter;
    private TwitterLoginButton btnTwitter;
    private TwitterSession data;

    public LoginFragment() {
    }

    public void setListener(FacebookListener listener) {
        this.listener = listener;
    }

    public void setListenerTwitter(TwitterListener listenerTwitter) {
        this.listenerTwitter = listenerTwitter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_login, container, false);

        btnTwitter = v.findViewById(R.id.btnTwitter);
        btnTwitter.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.v("TWITTEEEER", "ME HE LOGEADO----> ");
                data = result.data;
                listenerTwitter.onTWLogin(result.data);
                new TwitterApiClient(result.data).getAccountService().verifyCredentials(true, false, true).enqueue(new Callback<User>() {
                    @Override
                    public void success(Result<User> userResult) {
                        try {
                            User user = userResult.data;
                            fullname = user.name;
                            profilePictureS = user.profileImageUrl;
                            email = user.email;
                            gender = "Número de seguidores: " + user.followersCount;
                            birthday = "BIO: " + user.description;
                            //Llamo al método de iniciar sesion con Twitter
                            listenerTwitter.onTWLogin(data);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void failure(TwitterException e) {
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                Log.v("TWITTEEEER","EEERROOOOOOOR");
            }
        });

        callbackManager = CallbackManager.Factory.create();
        btnFacebook = v.findViewById(R.id.btnFacebook);
        btnFacebook.setReadPermissions("email","user_birthday");
        // If using in a fragment
        btnFacebook.setFragment(this);

        // Callback registration
        btnFacebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.v("Facebooook", "ME HE LOGEADO----> " + Profile.getCurrentProfile().getFirstName());
                GraphRequest request =
                        GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.e("FACEBOOOOOOK", object.toString());
                                Log.e("FACEBOOOOOOK", response.toString());
                                try {
                                    userId = object.getString("id");
                                    profilePicture = new URL("https://graph.facebook.com/" + userId + "/picture?width=500&height=500");
                                    if (object.has("first_name"))
                                        firstName = object.getString("first_name");
                                    if (object.has("last_name"))
                                        lastName = object.getString("last_name");
                                    if (object.has("email"))
                                        email = object.getString("email");
                                    if (object.has("birthday"))
                                        birthday = "F.Nacimiento: " + object.getString("birthday");
                                    if (object.has("gender"))
                                        gender = "Género: " + object.getString("gender");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (MalformedURLException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name, email, birthday, gender");
                request.setParameters(parameters);
                request.executeAsync();

                listener.onFBLogin(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.v("Facebooook","CANCELADO");
            }

            @Override
            public void onError(FacebookException exception) {
                Log.v("Facebooook","EEERROOOOOOOR " + exception );
            }
        });

        btnLogin = v.findViewById(R.id.btnLogin);
        btnRegister = v.findViewById(R.id.btnRegister);

        edtxt_User = v.findViewById(R.id.edtxt_user);
        edtxt_Password = v.findViewById(R.id.edtxt_pass);

        btnRegister.setOnClickListener( EventsAdmin.getInstance());
        btnLogin.setOnClickListener( EventsAdmin.getInstance());

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        btnTwitter.onActivityResult(requestCode,resultCode,data);
    }

    public TextInputEditText getEdtxt_User() {
        return edtxt_User;
    }

    public TextInputEditText getEdtxt_Password() {
        return edtxt_Password;
    }

    public Button getBtnLogin() {
        return btnLogin;
    }

    public Button getBtnRegister() {
        return btnRegister;
    }

    public LoginButton getBtnFacebook() {
        return btnFacebook;
    }

    public TwitterLoginButton getBtnTwitter() {
        return btnTwitter;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public void setProfilePictureS(String profilePictureS) {
        this.profilePictureS = profilePictureS;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public URL getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(URL profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getProfilePictureS() {
        return profilePictureS;
    }
}

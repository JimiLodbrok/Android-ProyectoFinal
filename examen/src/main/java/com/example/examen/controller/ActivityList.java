package com.example.examen.controller;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import com.example.examen.R;
import com.example.examen.model.firebase.FirebaseAdmin;
import com.example.examen.model.firebase.FirebaseDataListener;
import com.example.examen.utils.adapters.ListAdapter;
import com.example.examen.utils.adapters.ListaAdapterListener;
import com.example.examen.utils.cells.CeldaNoticia;
import com.example.mylibrary.fragments.ListFragment;

public class ActivityList extends AppCompatActivity implements FirebaseDataListener, ListaAdapterListener{
    private TextView display;
    private ListFragment listFragment;
    private ListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        /*
            Seteo el texto de textView poniendole el usuario que se ha logeado
         */
        display = findViewById(R.id.textDisplay);
        display.setText("Hello " + FirebaseAdmin.getInstance().getCurrentUser().getEmail().toString() + "!!!");

        listFragment = (ListFragment) getSupportFragmentManager().findFragmentById(R.id.list);

        //Seteo el listener del Firebase la parte de base de datos
        FirebaseAdmin.getInstance().setDataListener(this);
        adapter = new ListAdapter(this);
        adapter.setListener(this);

        FirebaseAdmin.getInstance().loadData("noticias");
        crearFragmentLista();
    }
    /*
        Cuando le doy atras y vuelvo al login, y vuelvo a meterme en la sesion, se llama a este método porque el activity ya esta creado
     */
    @Override
    protected void onResume() {
        super.onResume();
        dataLoaded();
        crearFragmentLista();
    }

    public void crearFragmentLista() {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.show(listFragment);
        transition.commit();
    }
    /*
        Actualiza el recyclerView pasandole el listAdapter actualizado
     */
    @Override
    public void dataLoaded() {
        listFragment.getRecyclerView().setAdapter(adapter);
    }

    @Override
    public void listaClicked(CeldaNoticia celda) {
        Log.v("CeldaNoticia","HE PRESIONADO SOBRE LA CELDA----------------" + celda);
    }
}

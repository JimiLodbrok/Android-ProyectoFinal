package com.example.examen.model.firebase;

/**
 * Created by jaime.castan on 19/12/2017.
 */

public interface FirebaseDataListener {
    void dataLoaded();
}

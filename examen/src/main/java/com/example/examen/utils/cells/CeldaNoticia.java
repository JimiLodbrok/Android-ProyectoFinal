package com.example.examen.utils.cells;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.examen.R;
import com.example.examen.utils.adapters.ListaAdapterListener;

/**
 * Created by jaime.castan on 19/12/2017.
 */

public class CeldaNoticia extends RecyclerView.ViewHolder implements View.OnClickListener {
    private TextView titulo;
    private ImageView imagen;
    private ListaAdapterListener listener;

    public CeldaNoticia(View itemView) {
        super(itemView);
        titulo = itemView.findViewById(R.id.titular);
        imagen = itemView.findViewById(R.id.imagen);
        itemView.setOnClickListener(this);
    }

    public void setListener(ListaAdapterListener listener) {
        this.listener = listener;
    }

    public void setTitulo(String titulo) {
        this.titulo.setText(titulo);
    }


    public ImageView getImagen() {
        return imagen;
    }

    @Override
    public void onClick(View v) {
        listener.listaClicked(this);
    }
}

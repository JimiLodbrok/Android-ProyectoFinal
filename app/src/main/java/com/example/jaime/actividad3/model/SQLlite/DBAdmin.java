package com.example.jaime.actividad3.model.SQLlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.jaime.actividad3.model.persistence.Jugador;

public class DBAdmin extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;

    private static final String DATABASE_NAME = "basket";
    private static final String TABLE_PLAYERS = "jugadores";

    private static final String COLUMN_KEY = "id";
    private static final String COLUMN_NAME = "nombre";
    private static final String COLUMN_HEIGHT = "altura";
    private static final String COLUMN_TEAM = "equipo";
    private static final String COLUMN_LATITUD = "lat";
    private static final String COLUMN_LONGITUD = "lon";
    private static final String COLUMN_URLIMAGE = "url";
    private static final String SQL_CREATE_TABLE_PLAYERS = "CREATE TABLE " + TABLE_PLAYERS + "(" + COLUMN_KEY +
            " INTEGER PRIMARY KEY," + COLUMN_NAME + " TEXT," +  COLUMN_TEAM + " TEXT," + COLUMN_HEIGHT + " DOUBLE," +
             COLUMN_URLIMAGE + " TEXT," + COLUMN_LATITUD + " DOUBLE," + COLUMN_LONGITUD
            + " DOUBLE" + ")";
    private static final String SQL_DELETE_TABLE = "DROP TABLE " + TABLE_PLAYERS;

    public DBAdmin(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_PLAYERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addJugador(Jugador jugador, String id) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_KEY, id);
        values.put(COLUMN_NAME, jugador.getNombre());
        values.put(COLUMN_TEAM, jugador.getEquipo());
        values.put(COLUMN_HEIGHT, String.valueOf(jugador.getAltura()));
        values.put(COLUMN_URLIMAGE, jugador.getUrlImage());
        values.put(COLUMN_LATITUD, String.valueOf(jugador.getLat()));
        values.put(COLUMN_LONGITUD, String.valueOf(jugador.getLon()));

        db.insert(TABLE_PLAYERS, null, values);
        db.close();
    }

    public Jugador getJugador(int id) {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.query(TABLE_PLAYERS, new String[]{
                COLUMN_KEY, COLUMN_NAME, COLUMN_TEAM, COLUMN_HEIGHT, COLUMN_URLIMAGE, COLUMN_LATITUD,
                COLUMN_LONGITUD}, COLUMN_KEY + "=?", new String[]{String.valueOf(id)},
                null, null, null, null);
        if(cursor != null)
            cursor.moveToFirst();

        Jugador jugador = new Jugador(cursor.getString(1), cursor.getString(2),
                cursor.getDouble(3), cursor.getString(4),
                cursor.getDouble(5), cursor.getDouble(6));
        Log.v("Activityliiiiiii", String.valueOf(id));
        Log.v("Activityliiiiiii", jugador.getNombre());
        return jugador;
    }

    public void deleteTable() {
        getReadableDatabase().execSQL(SQL_DELETE_TABLE);
    }

}

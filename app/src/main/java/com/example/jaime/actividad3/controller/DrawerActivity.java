package com.example.jaime.actividad3.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import com.bumptech.glide.Glide;
import com.example.jaime.actividad3.R;
import com.example.jaime.actividad3.model.SQLlite.DBAdmin;
import com.example.jaime.actividad3.model.firebase.FirebaseAdmin;
import com.example.jaime.actividad3.model.firebase.FirebaseDataListener;
import com.example.jaime.actividad3.model.persistence.Jugador;
import com.example.mylibrary.fragments.DetalleMapFragment;
import com.facebook.login.LoginManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.crash.FirebaseCrash;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener, FirebaseDataListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    private TextView nombre, email;
    private ImageView imgPerfil;


    private SupportMapFragment mapFragment;
    private DetalleMapFragment detMapFragment;

    private GoogleMap mMap;
    private DBAdmin dbAdmin;
    private int contador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        /*
            De esta manera puedo acceder a los views dentro del navigation drawer, para
            poder setear la foto, el nombre y el e-mail del usuario
         */
        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        nombre = header.findViewById(R.id.nombre);
        email = header.findViewById(R.id.email);
        imgPerfil = header.findViewById(R.id.imagenPerfil);
        navigationView.setNavigationItemSelectedListener(this);



        dbAdmin = new DBAdmin(this);
        FirebaseAdmin.getInstance().setDataListener(this);
        detMapFragment = (DetalleMapFragment) getSupportFragmentManager().findFragmentById(R.id.detalleMapFragment);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);

        ocultarDetalle();

        cargarDatosTwitter();

        crearMapaFragment();
    }

    /*
        Método que carga los datos de Firebase llamando al metodo loadData
     */
    public void crearMapaFragment() {
        FirebaseAdmin.getInstance().loadData("jugadores");
    }

    /*
        Método que oculta el fragment detalle del mapa
     */
    public void ocultarDetalle() {FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(detMapFragment);
        transition.commit();
    }

    /*
        Método que carga los datos sacados de la api de Twitter
        para msotrarlo en el drawer con el email, foto etc
        si no tiene foto de perfil, pone una genérica
     */
    public void cargarDatosTwitter() {
        Bundle inBundle = getIntent().getExtras();

        nombre.setText(inBundle.getString("name"));
        email.setText(inBundle.getString("email"));
        if(inBundle.getString("imageUrl")!= null)
            Glide.with(this).load(inBundle.getString("imageUrl")).into(imgPerfil);
        else
            Glide.with(this).load("https://cuadrosyvinilos.es/content/upload/master/thumb/470/59361fb8-0d4e-4ed5-a53a-6d0f91a16750.png").into(imgPerfil);
    }

    @Override
    public void onBackPressed() {
        drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            cerrarSesion();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.elem_menu1) {
            ocultarDetalle();
        } else if (id == R.id.elem_menu2) {

        } else if (id == R.id.elem_menu3) {
            cerrarSesion();
        }

        drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*
        Método que sirve para cerrar sesion de Firebase
        y hacer sign out de la aplicacion
     */
    public void cerrarSesion() {
        LoginManager.getInstance().logOut();
        FirebaseAdmin.getInstance().signOut();
        Intent secondActivity = new Intent(this, ActivityLogin.class);
        startActivity(secondActivity);
        finish();
    }

    /*
        Método que se ejecuta al detectar que se pulsa sobre un pin del mapa.
        Seteo los atributos del cardview con los datos del objeto jugador que está
        asociado a ese pin del mapa.
        Cargo las imagenes desde Firebase, y si no tiene le pone una por defecto.
        Después muestro el fragment
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        try {
            Jugador jugador = (Jugador) marker.getTag();
            Log.v("SecondActivity", "PRESIONADO EN PIIIIIN: " + jugador.getNombre());
            detMapFragment.getNombreJugador().setText(jugador.getNombre());
            detMapFragment.getAlturaJugador().setText(String.valueOf(jugador.getAltura()));
            detMapFragment.getEquipoJugador().setText(jugador.getEquipo());
            if(jugador.getUrlImage() != null)
                Glide.with(this).load(jugador.getUrlImage()).into(detMapFragment.getImagenJugador());
            else
                Glide.with(this).load("https://cuadrosyvinilos.es/content/upload/master/thumb/470/59361fb8-0d4e-4ed5-a53a-6d0f91a16750.png").into(detMapFragment.getImagenJugador());
            FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
            transition.show(detMapFragment);
            transition.commit();
        } catch (Exception ex) {
            FirebaseCrash.report(new Exception("Fallo al pinchar en un pin del mapa"));
        }
        return false;
    }

    /*
        Cuando el mapa se crea y está listo, se inicializa el google map
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
    }

    @Override
    public void dataLoaded() {

    }

    /*
            Método que sirve para pintar los marcadores en el mapa
            Añado tantos pines como valor tenga el parámetro contador(que se recibe desde FirebaseAdmin)
            Despues creo un jugador, y creo la latitud y longitud de su posicion para crear el marcador.
            Le pongo el titulo al marcador, seteando el nombre del jugador en cuestion
            Añado los marcadores después y les adjunto un tag, es decir, les asigno un objeto jugador para poder
            acceder a los datos al pinchar en el pin.
            Después hago zoom y se centra en el primer pin insertado.
         */
    @Override
    public void addMarkers(int contador) {
        try {
            this.contador = contador;
            for (int i = 0; i <= this.contador; i++) {
                Jugador jugador = dbAdmin.getJugador(i);
                LatLng position = new LatLng(jugador.getLat(), jugador.getLon());
                MarkerOptions markOp = new MarkerOptions();
                markOp.position(position);
                markOp.title(jugador.getNombre());
                if (mMap != null) {
                    jugador.setMarker(mMap.addMarker(markOp));
                    jugador.getMarker().setTag(jugador);
                    if (i == 0) mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position,4));
                }
            }
        } catch (Exception ex) {
            FirebaseCrash.report(new Exception("Fallo al añadir los pines al mapa"));
        }
    }

    /*
        Método que se usa para borrar todos los marcadores
        del mapa, para que no se repitan
     */
    @Override
    public void removeMarkers() {
        for (int i = 0; i < this.contador; i++) {
            Jugador jugador = dbAdmin.getJugador(i);
            if(jugador.getMarker() != null) {
                jugador.getMarker().remove();
            }
        }
    }

    /*
        Método que llama al método de SQLlite de añadir un jugador
        pasado por parámetro junto su id.
     */
    @Override
    public void addDataToSQL(Jugador jugador, String id) {
        dbAdmin.addJugador(jugador, id);
    }

    /*
        Método linkeado al xml que muestra un mensaje en un snackbar
     */
    public void mensajeFab(View view) {
        Snackbar.make(view, "Hola " + nombre.getText() + ", ¿que tal llevas el dia?", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    /*
        Método linkeado al xml que hace una animacion

    public void animacion(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(this, R.anim.btn_twitter_animation));
    }
    */

}

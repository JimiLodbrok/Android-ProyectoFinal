package com.example.jaime.actividad3.model.firebase;

public interface FirebaseLoginListener {
    void userSignedIn();
    void userCreated();
    void fbUserSignIn();
    void twtUserSignIn();
}

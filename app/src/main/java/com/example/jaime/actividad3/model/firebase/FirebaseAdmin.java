package com.example.jaime.actividad3.model.firebase;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.jaime.actividad3.model.SQLlite.DBAdmin;
import com.example.jaime.actividad3.model.instance.DataHolder;
import com.example.jaime.actividad3.model.persistence.Jugador;
import com.facebook.AccessToken;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.TwitterAuthProvider;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.twitter.sdk.android.core.TwitterSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.ContentValues.TAG;

public class FirebaseAdmin {
    private static final FirebaseAdmin instance = new FirebaseAdmin();
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference mDatabase;
    private Activity activity;
    private FirebaseUser user;
    private FirebaseLoginListener listenerLogin;
    private FirebaseDataListener listenerData;
    private int contador;

    public FirebaseAdmin() {
        mAuth = FirebaseAuth.getInstance();
        database = FirebaseDatabase.getInstance();
        mDatabase = database.getReference();
    }

    public static FirebaseAdmin getInstance() {
        return instance;
    }

    public void setLoginListener(FirebaseLoginListener listenerLogin) {
        this.listenerLogin = listenerLogin;
    }

    public void setDataListener(FirebaseDataListener listenerData) {
        this.listenerData = listenerData;
    }

    public boolean checkAccount() {
        if (user != null)
            return true;
        else
            return false;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void createAccount(String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(activity, "User created.",
                                    Toast.LENGTH_SHORT).show();
                            Log.d(TAG, "createUserWithEmail:success");
                            listenerLogin.userCreated();
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        }
                    }
                });
    }

    public void signIn(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            Log.e(TAG, "seeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
                            user = mAuth.getCurrentUser();
                            System.out.println(user.getEmail().toString());
                            listenerLogin.userSignedIn();
                            FirebaseCrash.log("Activity login se ha logeado correctamente");
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Log.e(TAG, "nooooooooooooooooooooooooooooooooo ......................");
                            Toast.makeText(activity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            user = mAuth.getCurrentUser();
                            listenerLogin.fbUserSignIn();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(activity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void handleTwitterSession(TwitterSession session) {
        Log.d(TAG, "handleTwitterSession:" + session);
        AuthCredential credential = TwitterAuthProvider.getCredential(
                session.getAuthToken().token,
                session.getAuthToken().secret);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            user = mAuth.getCurrentUser();
                            listenerLogin.twtUserSignIn();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(activity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public void signOut() {
        mAuth.signOut();
    }

    public void loadData(String rama) {
        DatabaseReference databaseRef = mDatabase.child(rama);
        databaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(DataHolder.getInstance().getJugadores() != null) listenerData.removeMarkers();
                Log.v("ActivityList","jugadores---->" + dataSnapshot);
                GenericTypeIndicator<ArrayList<Jugador>> indicator = new GenericTypeIndicator<ArrayList<Jugador>>(){};
                for(int i = 0; i < dataSnapshot.getValue(indicator).size(); i++) {
                    listenerData.addDataToSQL(dataSnapshot.getValue(indicator).get(i), String.valueOf(i));
                    Log.d(TAG, "Value is: " + dataSnapshot.getValue(indicator).get(i).getNombre());
                    contador = i;
                }
                //DataHolder.getInstance().setJugadores(dataSnapshot.getValue(indicator));
                //Log.d(TAG, "Value is: " + DataHolder.getInstance().getJugadores().get(0).getNombre());
                //Log.d(TAG, "SIIIIIIII------>>>: " + DataHolder.getInstance().getJugadores().get(0).getLat());
                listenerData.addMarkers(contador);
                //listenerData.dataLoaded();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public void insertData(String rama, Map<String,Object> data) {
        Map<String,Object> childUpdates = new HashMap<>();
        childUpdates.put(rama,data);

        mDatabase.updateChildren(childUpdates);
    }

    public FirebaseUser getCurrentUser() {
        return user;
    }

}

package com.example.jaime.actividad3.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.example.jaime.actividad3.R;
import com.example.jaime.actividad3.model.firebase.FirebaseAdmin;
import com.example.jaime.actividad3.model.firebase.FirebaseLoginListener;
import com.example.mylibrary.events.EventsAdmin;
import com.example.mylibrary.events.EventsListener;
import com.example.mylibrary.fragments.FacebookListener;
import com.example.mylibrary.fragments.LoginFragment;
import com.example.mylibrary.fragments.RegisterFragment;
import com.example.mylibrary.fragments.TwitterListener;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.firebase.crash.FirebaseCrash;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterSession;

public class ActivityLogin extends AppCompatActivity implements EventsListener, FirebaseLoginListener, FacebookListener, TwitterListener {
    private LoginFragment loginFragment;
    private RegisterFragment registerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Twitter.initialize(this);
        setContentView(R.layout.activity_login);

        loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.loginFragment);
        registerFragment = (RegisterFragment) getSupportFragmentManager().findFragmentById(R.id.registerFragment);

        loginFragment.setListener(this);
        loginFragment.setListenerTwitter(this);

        loginFragment.getBtnTwitter().startAnimation(AnimationUtils.loadAnimation(this, com.example.mylibrary.R.anim.fade));
        loginFragment.getBtnFacebook().startAnimation(AnimationUtils.loadAnimation(this, com.example.mylibrary.R.anim.move));
        loginFragment.getBtnLogin().startAnimation(AnimationUtils.loadAnimation(this, com.example.mylibrary.R.anim.rotate));
        loginFragment.getBtnRegister().startAnimation(AnimationUtils.loadAnimation(this, com.example.mylibrary.R.anim.rotate));

        LoginManager.getInstance().logOut();

        FirebaseAdmin.getInstance().setLoginListener(this);
        FirebaseAdmin.getInstance().setActivity(this);
        EventsAdmin.getInstance().addListener(this);

        onReturnClicker();

        FirebaseCrash.log("Activity login created");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginFragment.onActivityResult(requestCode,resultCode,data);
    }

    public void onLoginClicked() {
        if (!loginFragment.getEdtxt_User().getText().toString().equals("") && !loginFragment.getEdtxt_Password().getText().toString().equals(""))
            FirebaseAdmin.getInstance().signIn(loginFragment.getEdtxt_User().getText().toString(), loginFragment.getEdtxt_Password().getText().toString());
        else
            Toast.makeText(this, "Fill the sign in fields",
                    Toast.LENGTH_SHORT).show();

    }

    public void onNewRegisterClicker() {
        if (!registerFragment.getEdtxt_NewUser().getText().toString().equals("") && !registerFragment.getEdtxt_NewPassword().getText().toString().equals(""))
            if (registerFragment.getEdtxt_NewPassword().getText().toString().equals(registerFragment.getEdtxt_NewPassword2().getText().toString())) {
                loginFragment.getEdtxt_User().setText(registerFragment.getEdtxt_NewUser().getText().toString());
                FirebaseAdmin.getInstance().createAccount(registerFragment.getEdtxt_NewUser().getText().toString(), registerFragment.getEdtxt_NewPassword().getText().toString());
            } else
                Toast.makeText(this, "The passwords have to match",
                        Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Pleas fill all the fields.",
                    Toast.LENGTH_SHORT).show();
    }

    public void onRegisterClicked() {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(loginFragment);
        transition.show(registerFragment);
        transition.commit();
        clearLoginFragment();
    }

    public void onReturnClicker() {
        FragmentTransaction transition = getSupportFragmentManager().beginTransaction();
        transition.hide(registerFragment);
        transition.show(loginFragment);
        transition.commit();
        clearRegisterFragment();
    }

    public void clearLoginFragment() {
        loginFragment.getEdtxt_User().setText("");
        loginFragment.getEdtxt_Password().setText("");
    }

    public void clearRegisterFragment() {
        registerFragment.getEdtxt_NewUser().setText("");
        registerFragment.getEdtxt_NewPassword().setText("");
        registerFragment.getEdtxt_NewPassword2().setText("");
    }

    @Override
    public void onClickScren(View view) {
        if (view.getId() == R.id.btnLogin) {
            onLoginClicked();
        } else if (view.getId() == R.id.btnRegister)
            onRegisterClicked();
        else if (view.getId() == R.id.btnNRegister) {
            onNewRegisterClicker();
        } else if (view.getId() == R.id.btnVolver)
            onReturnClicker();
    }

    @Override
    public void userSignedIn() {
        Intent secondActivity = new Intent(this, DrawerActivity.class);
        secondActivity.putExtra("name", FirebaseAdmin.getInstance().getCurrentUser().getDisplayName());
        secondActivity.putExtra("email", FirebaseAdmin.getInstance().getCurrentUser().getEmail());
        secondActivity.putExtra("imageUrl", FirebaseAdmin.getInstance().getCurrentUser().getPhotoUrl());
        startActivity(secondActivity);
        FirebaseCrash.log("Activity login hace transicion a Activity List");
    }

    @Override
    public void userCreated() {
        onReturnClicker();
        clearRegisterFragment();
    }

    @Override
    public void fbUserSignIn() {
        Intent secondActivity = new Intent(this, DrawerActivity.class);
        secondActivity.putExtra("name", loginFragment.getFirstName() + " " + loginFragment.getLastName());
        //secondActivity.putExtra("surname", loginFragment.getLastName());
        secondActivity.putExtra("birthday", loginFragment.getBirthday());
        secondActivity.putExtra("email", loginFragment.getEmail());
        secondActivity.putExtra("uid", loginFragment.getUserId());
        secondActivity.putExtra("gender", loginFragment.getGender());
        secondActivity.putExtra("imageUrl", loginFragment.getProfilePicture().toString());
        startActivity(secondActivity);
        finish();
    }

    @Override
    public void twtUserSignIn() {
        try {
            Intent secondActivity = new Intent(this, DrawerActivity.class);
            secondActivity.putExtra("name", loginFragment.getFullname());
            secondActivity.putExtra("birthday", loginFragment.getBirthday());
            secondActivity.putExtra("email", loginFragment.getEmail());
            secondActivity.putExtra("gender", loginFragment.getGender());
            secondActivity.putExtra("imageUrl", loginFragment.getProfilePictureS());
            startActivity(secondActivity);
        } catch (Exception ex) {
            FirebaseCrash.report(new Exception("Fallo al coger informacion del Twitter"));
        }
    }

    @Override
    public void onFBLogin(AccessToken token) {
        FirebaseAdmin.getInstance().handleFacebookAccessToken(token);
    }

    @Override
    public void onTWLogin(TwitterSession data) {
        FirebaseAdmin.getInstance().handleTwitterSession(data);
    }

}

package com.example.jaime.actividad3.model.persistence;

import com.google.android.gms.maps.model.Marker;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Jugador {
    private String nombre;
    private String equipo;
    private Double altura;
    private String urlImage;
    private Double lat,lon;
    private Marker marker;

    public Jugador() {

    }

    public Jugador(String nombre, String equipo, Double altura, String urlImage, Double lat, Double lon) {
        this.nombre = nombre;
        this.equipo = equipo;
        this.altura = altura;
        this.urlImage = urlImage;
        this.lat = lat;
        this.lon = lon;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }
}
